<?php

declare (strict_types=1);

namespace magein\think\utils\service;

use think\Service;

class UService extends Service
{
    /**
     * 注册服务
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * 执行服务
     *
     * @return void
     */
    public function boot()
    {
        // 加载函数
        $this->loadFunc();

        // 注册路由
        $this->loadRoutesFrom(__DIR__ . '/route.php');
    }

    protected function loadFunc()
    {
        include __DIR__ . '/common.php';
    }
}