<?php

declare (strict_types=1);

namespace magein\think\utils\service;

use think\facade\Cache;
use think\Request;

class User
{
    /**
     * 获取请求中的用户id
     * @param Request|null $request
     * @return mixed
     */
    public static function id(Request $request = null)
    {
        if ($request) {
            $authorization = $request->header('authorization');
        } else {
            $authorization = request()->header('authorization');
        }

        if ($authorization) {
            return cache($authorization);
        }

        return null;
    }

    /**
     * 设置token
     * @param $id
     * @param int $expire_time
     * @return bool
     */
    public static function token($id, int $expire_time = 86400): bool
    {
        if (empty($id)) {
            return false;
        }

        $token = md5(uniqid() . $id);

        if ($expire_time < 1800) {
            $expire_time = 1800;
        }

        return Cache::tag('user_login')->set($token, $id, $expire_time);
    }
}

