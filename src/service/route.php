<?php

/**
 * @user magein
 * @date 2023/11/30 16:19
 */

use magein\think\utils\service\User;
use think\facade\Route;

Route::any('repos/:name/[:action]', '\magein\think\utils\ApiRepos@entry')
    ->append(['user_id' => User::id()]);