<?php

if (!function_exists('is_local')) {
    /**
     * 判断是否是本地环境
     * @return bool
     */
    function is_local(): bool
    {
        return env('app_env') === 'local';
    }
}

if (!function_exists('is_dev')) {
    /**
     * 判断是否是开发环境
     * @return bool
     */
    function is_dev(): bool
    {
        return env('app_env') === 'dev';
    }
}

if (!function_exists('is_production')) {
    /**
     * 判断是否是生产环境
     * @return bool
     */
    function is_production(): bool
    {
        $env = env('app_env');
        if (in_array($env, ['production', 'produce', 'prod'])) {
            return true;
        }
        return false;
    }
}

if (!function_exists('now')) {
    /**
     * 获取当前时间
     * @return string
     */
    function now(): string
    {
        return date('Y-m-d H:i:s');
    }
}

if (!function_exists('datetime')) {
    /**
     * 获取当前时间
     * @return string
     */
    function datetime(): string
    {
        return date('Y-m-d H:i:s');
    }
}