<?php

/**
 * @user magein
 * @date 2023/12/1 15:50
 */

namespace magein\think\utils\constants;

class StatusConst
{
    const FORBID = 0;

    const OPEN = 1;

    const NO = 0;

    const YES = 1;

    const DOWN = 0;

    const UP = 1;
}