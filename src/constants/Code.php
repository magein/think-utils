<?php

/**
 * @user magein
 * @date 2023/12/5 17:28
 */

namespace magein\think\utils\constants;

class Code
{
    // 授权为空（需要登录）
    const AUTHORIZATION_EMPTY = 100101;

    // 授权信息非法
    const AUTHORIZATION_ILLEGAL = 100103;

    /**
     * 数据仓库代码
     */

    // 数据仓库未匹配
    const   REPOS_NAME_EMPTY = 300101;

    // 数据仓库设置错误，需要继承magein\think\utils\Repository\Repository
    const   REPOS_NO_FOUND = 300103;

    // 数据仓库未设置模型，或者模型没有继承think\model
    const   REPOS_NO_MODEL = 300105;

    // 数据仓库行为错误
    const   REPOS_ACTION_ERROR = 300107;

    // 数据仓库不允许执行新增、更新操作
    const   REPOS_NOT_SUPPORT_SAVE = 300109;
    // 更新或者新增参数为空
    const   REPOS_SAVE_PARAMS_EMPTY = 300111;


    // 数据仓库字段验证类设置错误
    const REPOS_VALIDATE_EMPTY = 300201;

    // 数据仓库验证类没有继承think\Validate
    const REPOS_VALIDATE_EXTEND_ERROR = 300203;

    // 不允许执行删除
    const  REPOS_DELETE_REFUSED = 300301;

    // 删除约束条件为空
    const  REPOS_DELETE_RESTRAIN_EMPTY = 300303;

    // 没有找到数据记录， 需要检测传递的参数
    const  REPOS_DELETE_EMPTY_DATA = 300305;

    // 服务系统执行错误
    const SERVER_SYSTEM_ERROR = 4000100;
    // 数据库出现错误
    const  DB_QUERY_ERROR = 400101;
    // 表单验证失败
    const VALIDATE_ERROR = 400103;
    // 路由不匹配
    const ROUTE_NOT_FOUND = 400105;

}