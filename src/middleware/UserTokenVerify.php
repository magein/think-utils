<?php
declare (strict_types=1);

namespace magein\think\utils\middleware;

use magein\think\utils\ApiResponse;
use magein\think\utils\constants\Code;
use magein\think\utils\service\User;
use think\Response;

class UserTokenVerify
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $authorization = $request->header('authorization');
        if (empty($authorization)) {
            return ApiResponse::error('非法请求', Code::AUTHORIZATION_EMPTY);
        }

        $user_id = User::id($request);
        if (empty($user_id)) {
            return ApiResponse::error('非法请求', Code::AUTHORIZATION_ILLEGAL);
        }

        return $next($request);
    }
}
