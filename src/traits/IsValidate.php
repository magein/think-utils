<?php

/**
 * @user magein
 * @date 2023/12/1 15:48
 */

namespace magein\think\utils\traits;

use magein\think\utils\constants\StatusConst;

trait IsValidate
{
    /**
     * 验证时候已经到期
     * @return bool
     */
    public function isVlidated()
    {
        $data = $this->getData();
        if (empty($data)) {
            return false;
        }

        if (isset($data['status']) && $data['status'] == StatusConst::FORBID) {
            return false;
        }

        // 尚未开始
        if (($data['begin_time'] ?? '') && strtotime($data['begin_time']) > time()) {
            return false;
        }

        // 尚未开始
        if (($data['start_time'] ?? '') && strtotime($data['begin_time']) > time()) {
            return false;
        }

        // 已经结束
        if (($data['end_time'] ?? '') && strtotime($data['end_time']) < time()) {
            return false;
        }

        return true;
    }
}