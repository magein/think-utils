<?php

/**
 * @user magein
 * @date 2023/12/1 15:48
 */

namespace magein\think\utils\traits;

use magein\think\utils\constants\StatusConst;

trait StatusTranslate
{
    /**
     * @param $value
     * @return int
     */
    public function setStatusAttr($value): int
    {
        return intval($value);
    }

    /**
     * @param $value
     * @return string|array
     */
    public static function translateStatus($value = null)
    {
        $data = [
            StatusConst::FORBID => '禁用',
            StatusConst::OPEN => '启用'
        ];

        if ($value !== null) {
            return $data[$value] ?? '';
        }

        return $data;
    }

    /**
     * @param $value
     * @return string|array
     */
    public static function translateYesOrNo($value = null)
    {
        $data = [
            StatusConst::NO => '否',
            StatusConst::YES => '是'
        ];

        if ($value !== null) {
            return $data[$value] ?? '';
        }

        return $data;
    }

    /**
     * @param $value
     * @return string|array
     */
    public static function translateUpOrDown($value = null)
    {
        $data = [
            StatusConst::DOWN => '下架',
            StatusConst::UP => '上架'
        ];

        if ($value !== null) {
            return $data[$value] ?? '';
        }

        return $data;
    }

    /**
     * @param $value
     * @param $data
     * @return string
     */
    public function getStatusTextAttr($value, $data)
    {
        $value = $data['status'] ?? null;
        if ($value === null) {
            return '';
        }
        return self::translateStatus($value);
    }
}