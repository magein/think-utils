<?php

/**
 * @user magein
 * @date 2023/12/1 15:44
 */

namespace magein\think\utils;

use magein\utils\Variable;
use think\Model;

/**
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @method static where($field, $op = null, $condition = null)
 * @method static find($data = null)
 */
class MainModel extends Model
{
    protected $autoWriteTimestamp = 'datetime';

    protected $createTime = 'created_at';

    protected $updateTime = 'updated_at';

    public $hidden = [
        'updated_at',
        'deleted_at'
    ];

    public static function __callStatic($name, $arguments)
    {
        preg_match('/^_{1,3}/', $name, $matches);

        $len = strlen($matches[0] ?? '');

        if ($len > 0) {
            $field = trim(substr($name, $len), '_');
            $field = Variable::ins()->underline($field);

            if (empty($field) || empty($arguments)) {
                return null;
            }
            $value = $arguments[0] ?? null;
            if (empty($value)) {
                return null;
            }
            $params = $arguments[1] ?? [];
            if (!is_array($params)) {
                $params = [];
            }

            $params[$field] = $value;
            if ($len === 1) {
                return static::where($params)->find();
            } elseif ($len === 2) {
                if (isset($arguments[1]) && is_int($arguments[1])) {
                    $page_size = $arguments[1];
                } elseif ($params['page_size'] ?? '') {
                    $page_size = $params['page_size'];
                    unset($params['page_size']);
                } else {
                    $page_size = request()->param('page_size', 15);
                }

                if ($page_size <= 0) {
                    $page_size = 15;
                }

                if ($page_size > 200) {
                    $page_size = 200;
                }

                return static::where($params)->paginate([
                    'list_rows' => $page_size,
                    'var_page' => 'page_id',
                ]);
            } elseif ($len === 3) {
                return static::where($params)->select();
            }
        }

        return parent::__callStatic($name, $arguments);
    }

    /**
     * 检索一个模型数据
     * @param $where
     * @param $params
     * @return bool|MainModel|Model|static
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function firstOrCreate($where, $params = [])
    {
        $model = (new static())->where($where)->find();

        if (empty($model)) {
            return static::create(array_merge($where, $params));
        }

        return $model;
    }

    /**
     * @param $where
     * @param $params
     * @return MainModel|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function updateOrCreate($where, $params = [])
    {
        $model = (new static())->where($where)->find();

        if (empty($model)) {
            return static::create(array_merge($where, $params));
        }

        return static::update($params, $where);
    }
}