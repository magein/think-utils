<?php

namespace magein\think\utils;

use magein\utils\Result;
use think\Response;

/**
 * @user magein
 * @date 2023/11/30 17:19
 */
class ApiResponse
{
    /**
     * @param Result|array|string|int $result
     * @param array $header
     * @return Response
     */
    public static function result($result, array $header = [])
    {
        if (!$result instanceof Result) {
            if ($result === false) {
                $result = Result::error();
            } else {
                $result = Result::success($result);
            }
        }

        return Response::create($result->toArray(), 'json')->header($header);
    }

    /**
     * @param $message
     * @param $code
     * @param $data
     * @return Response
     */
    public static function error($message, $code = 1, $data = null)
    {
        return self::result(Result::error($message, $code, $data));
    }

    /**
     * @param $data
     * @param $message
     * @param $code
     * @return Response
     */
    public static function success($data = null, $message = '', $code = 0)
    {
        return self::result(Result::success($data, $message, $code));
    }
}