<?php

/**
 * @user magein
 * @date 2023/12/9 12:58
 */

namespace magein\think\utils;

use think\paginator\driver\Bootstrap;

class ApiPaginate
{
    /**
     * @param Bootstrap $data
     * @return array
     */
    public static function api(Bootstrap $data): array
    {
        $page['page_id'] = $data->currentPage();
        $page['last_page'] = $data->lastPage();
        $page['total'] = $data->total();
        $page['page_size'] = $data->listRows();
        $page['has_more'] = $data->hasPages();

        $lists = $data->items();

        return compact('page', 'lists');
    }

    /**
     * @return array
     */
    public static function config(): array
    {
        $page_size = (request()->param('page_size') ?? 0) ?: 15;
        if ($page_size > 200) {
            $page_size = 200;
        }

        return [
            'list_rows' => $page_size,
            'var_page' => 'page_id',
        ];
    }
}