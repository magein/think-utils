<?php

/**
 * @user magein
 * @date 2023/12/1 15:44
 */

namespace magein\think\utils;

use think\model\concern\SoftDelete;

/**
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class SoftModel extends MainModel
{
    use SoftDelete;

    protected $deleteTime = 'deleted_at';
}