<?php

/**
 * @user magein
 * @date 2023/12/7 9:17
 */

namespace magein\think\utils\repository;

class DeleteSetting
{

    /**
     * 允许删除操作
     * @var bool
     */
    protected $allow = false;

    /**
     * 用户标记用户关联的字段
     * @var string
     */
    protected $user_id = 'user_id';

    /**
     * 用于接受的主键
     * @var string
     */
    protected $primary_key = 'id';

    /**
     * 允许批量删除
     * @var bool
     */
    protected $batch = false;

    /**
     * @var null|callable
     */
    protected $before = null;

    /**
     * 删除条件
     * @var array
     */
    protected $condition = [];

    /**
     * 软删除
     * @var bool
     */
    protected $soft = true;

    /**
     * @param bool $allow
     * @return bool
     */
    public function allow(bool $allow = true)
    {
        return $this->allow = $allow;
    }

    /**
     * @return bool
     */
    public function isAllow(): bool
    {
        return $this->allow;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     * @return void
     */
    public function setUserId(string $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getPrimaryKey(): string
    {
        return $this->primary_key;
    }

    /**
     * @param string $primary_key
     * @return void
     */
    public function setPrimaryKey(string $primary_key): void
    {
        $this->primary_key = $primary_key;
    }

    /**
     * @return bool
     */
    public function isBatch(): bool
    {
        return $this->batch;
    }

    /**
     * @param bool $batch
     * @return void
     */
    public function setBatch(bool $batch = true): void
    {
        $this->batch = $batch;
    }

    /**
     * @return callable
     */
    public function getBefore(): ?callable
    {
        return $this->before;
    }

    /**
     * @param callable $before
     * @return void
     */
    public function before(callable $before): void
    {
        $this->before = $before;
    }

    /**
     * @return array
     */
    public function getCondition(): array
    {
        return $this->condition;
    }

    /**
     * @param array $conditions
     * @return void
     */
    public function setConditions(array $conditions)
    {
        foreach ($conditions as $key => $item) {
            if (is_array($item)) {
                $this->condition[] = $item;
            } else {
                $this->condition[] = [$key, '=', $item];
            }
        }
    }

    /**
     * @param string $field
     * @param $express
     * @param $value
     * @return void
     */
    public function setCondition(string $field, $express = null, $value = null): void
    {
        if (is_null($value) && !is_null($express)) {
            $value = $express;
            $express = '=';
        }

        $this->condition[] = [$field, $express, $value];
    }

    /**
     * @return bool
     */
    public function isSoft(): bool
    {
        return $this->soft;
    }

    /**
     * @param bool $soft
     * @return void
     */
    public function setSoft(bool $soft = true): void
    {
        $this->soft = $soft;
    }
}