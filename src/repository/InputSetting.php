<?php

/**
 * @user magein
 * @date 2023/12/7 9:17
 */

namespace magein\think\utils\repository;

class InputSetting
{
    /**
     * 接收参数
     * @var array
     */
    protected $accept = [];

    /**
     * 系统填充的表单信息，用于跟接收到的合并
     * @var array
     */
    protected $data = [];

    /**
     * 验证类
     * @var string
     */
    protected $validate = null;

    /**
     * 更新数据用到的字段，需要包含这个字段且不为空
     * @var string
     */
    protected $primary_key = 'id';

    /**
     * 允许调用save
     * @var bool
     */
    protected $allow_save = false;

    /**
     * 允许调用create
     * @var bool
     */
    protected $allow_create = false;

    /**
     * @var null
     */
    protected $user = null;

    /**
     * @var array
     */
    protected $before = [];

    /**
     * @return array
     */
    public function getAccept(): array
    {
        return $this->accept;
    }

    /**
     * @param array $accept
     * @return void
     */
    public function setAccept(array $accept): void
    {
        $this->accept = $accept;
    }

    /**
     * @return string|null
     */
    public function getValidate(): ?string
    {
        return $this->validate;
    }

    /**
     * @param string|null $validate
     * @return void
     */
    public function setValidate(?string $validate): void
    {
        $this->validate = $validate;
    }

    /**
     * @return string
     */
    public function getPrimaryKey(): string
    {
        return $this->primary_key;
    }

    /**
     * @param string $primary_key
     * @return void
     */
    public function setPrimaryKey(string $primary_key): void
    {
        $this->primary_key = $primary_key;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return void
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param null $user
     */
    public function setUser($user = 'user_id'): void
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function isAllowSave(): bool
    {
        return $this->allow_save;
    }

    /**
     * @param bool $allow_save
     * @return void
     */
    public function allowSave(bool $allow_save = true): void
    {
        $this->allow_save = $allow_save;
    }

    /**
     * @return bool
     */
    public function isAllowCreate(): bool
    {
        return $this->allow_create;
    }

    /**
     * @param bool $allow_create
     * @return void
     */
    public function allowCreate(bool $allow_create = true): void
    {
        $this->allow_create = $allow_create;
    }

    /**
     * @param callable $before
     * @return void
     */
    public function createBefore(callable $before)
    {
        $this->before['create'] = $before;
    }

    /**
     * @param callable $before
     * @return void
     */
    public function updateBefore(callable $before)
    {
        $this->before['update'] = $before;
    }

    /**
     * @return array
     */
    public function getBefore()
    {
        return $this->before;
    }
}