<?php

/**
 * @user magein
 * @date 2023/12/5 17:28
 */

namespace magein\think\utils\repository;

class OutputSetting
{
    /**
     * @var string
     */
    protected $primary_key = 'id';

    /**
     * 对外暴露的字段
     * @var array
     */
    protected $fields = [];

    /**
     * 默认的过滤条件
     * @var int|null
     */
    protected $condition = [];

    /**
     * 单个查询的过滤条件
     *
     * 如
     * [
     *    ['status','=',1]
     * ]
     *
     * @var array
     */
    protected $single = [];

    /**
     * 关联模型
     * @var array
     */
    protected $with = [];

    /**
     * @var bool
     */
    protected $paginate = false;

    /**
     * 用于约束用户的相关查询
     * @var null
     */
    protected $user = null;

    /**
     * 排序
     * @var string|null
     */
    protected $sort = 'id desc';

    /**
     * 输出给前端前的回调
     * @var array
     */
    protected $before = [];

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     * @return void
     */
    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }

    /**
     * @return array|int|null
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param array $conditions
     * @return void
     */
    public function setConditions(array $conditions)
    {
        foreach ($conditions as $key => $item) {
            if (is_array($item)) {
                $this->condition[] = $item;
            } else {
                $this->condition[] = [$key, '=', $item];
            }
        }
    }

    /**
     * @param string $field
     * @param $express
     * @param $value
     * @return void
     */
    public function setCondition(string $field, $express = null, $value = null): void
    {
        if (is_null($value) && !is_null($express)) {
            $value = $express;
            $express = '=';
        }

        $this->condition[] = [$field, $express, $value];
    }

    /**
     * @return array
     */
    public function getSingle(): array
    {
        return $this->single;
    }

    /**
     * @param array $conditions
     * @return void
     */
    public function setSingles(array $conditions)
    {
        foreach ($conditions as $key => $item) {
            if (is_array($item)) {
                $this->single[] = $item;
            } else {
                $this->single[] = [$key, '=', $item];
            }
        }
    }

    /**
     * @param string $field
     * @param $express
     * @param $value
     * @return void
     */
    public function setSingle(string $field, $express = null, $value = null): void
    {
        if (is_null($value) && !is_null($express)) {
            $value = $express;
            $express = '=';
        }

        $this->single[] = [$field, $express, $value];
    }

    /**
     * @return string|null
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     * @return void
     */
    public function setSort(string $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return bool
     */
    public function isPaginate(): bool
    {
        return $this->paginate;
    }

    /**
     * @param bool $paginate
     * @return void
     */
    public function setPaginate(bool $paginate = true): void
    {
        $this->paginate = $paginate;
    }

    /**
     * @return string
     */
    public function getPrimaryKey(): string
    {
        return $this->primary_key;
    }

    /**
     * @param string $primary_key
     * @return void
     */
    public function setPrimaryKey(string $primary_key): void
    {
        $this->primary_key = $primary_key;
    }

    /**
     * @return array
     */
    public function getWith(): array
    {
        return $this->with;
    }

    /**
     * @param array $with
     * @return void
     */
    public function setWith(array $with): void
    {
        $this->with = $with;
    }

    /**
     * @return null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param null $user
     */
    public function setUser($user = 'user_id'): void
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getBefore(): array
    {
        return $this->before;
    }

    /**
     * @param callable $before
     * @return void
     */
    public function before(callable $before): void
    {
        $this->before['get'] = $before;
        $this->before['lists'] = $before;
    }

    /**
     * @param callable $before
     * @return void
     */
    public function simpleBefore(callable $before)
    {
        $this->before['get'] = $before;
    }

    /**
     * @param callable $before
     * @return void
     */
    public function listsBefore(callable $before)
    {
        $this->before['lists'] = $before;
    }
}