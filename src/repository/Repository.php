<?php

/**
 * @user magein
 * @date 2023/12/7 9:13
 */

namespace magein\think\utils\repository;

abstract class Repository
{

    /**
     * @return string
     */
    public abstract function model(): string;

    /**
     * @return OutputSetting
     */
    public abstract function output(): OutputSetting;

    /**
     * @return InputSetting
     */
    public abstract function input(): InputSetting;

    /**
     * @return DeleteSetting
     */
    public abstract function delete(): DeleteSetting;
}