<?php


namespace magein\think\utils\tests;

use magein\utils\Http;

trait TestHttp
{
    /**
     * @var Http|null
     */
    public $http = null;

    public function setUp()
    {
        $this->http = new Http();
    }

    /**
     * @param $route
     * @return string
     */
    protected function url($route): string
    {
        $host = $_ENV['phpunit_api_host'] ?? '';
        $host = trim($host);
        return $host . '/' . trim(trim($route), '/');
    }

    /**
     * @param string $name
     * @param string $repository
     * @return void
     * @throws \Exception
     */
    protected function reposOnlyOutput(string $name, string $repository)
    {
        $res = $this->http->get($this->url('repos/' . trim($name . '/')));
        $this->assertEquals(0, $res->getCode(), $res->getMsg());

        $repos = new $repository();
        $setting = $repos->input();
        $this->assertFalse($setting->isAllowSave(), $repository . '数据仓库必须禁止save操作。实际设置允许');
        $this->assertFalse($setting->isAllowCreate(), $repository . '数据仓库必须禁止create操作。实际设置允许');

        $setting = $repos->delete();
        $this->assertFalse($setting->isAllow(), $repository . '数据仓库必须禁止delete操作。实际设置允许');
    }
}