<?php

/**
 * @user magein
 * @date 2023/12/5 18:59
 */

namespace magein\think\utils;

use magein\utils\Result;
use magein\think\utils\constants\Code;
use magein\think\utils\repository\DeleteSetting;
use magein\think\utils\repository\InputSetting;
use magein\think\utils\repository\OutputSetting;
use magein\think\utils\repository\Repository;
use think\facade\Request;
use think\Model;
use think\model\Collection;
use think\Response;
use think\Validate;

/**
 * 接口中日常对数据库进行操作，这里是代理不用重复书写路由文件等
 */
class ApiRepos
{

    /**
     * 用户标记
     * @var string
     */
    protected $user_id = '';

    /**
     * @return Response
     */
    public function entry(): Response
    {
        $name = Request::route('name');
        $action = Request::route('action', '');
        $this->user_id = abs(intval(Request::route('user_id', '')));

        if (strtolower(Request::method()) === 'post') {
            $params = Request::post();
        } else {
            $params = Request::get();
        }

        $log = app()->config->get('repos.log.close');
        if ($log) {
            $this->log($name, $action, $this->user_id, $params);
        }

        return ApiResponse::result($this->parse($name, $action, $params));
    }

    /**
     * @param $name
     * @param $action
     * @param array $params
     * @return Result
     */
    protected function parse($name, $action, array $params = []): Result
    {
        if (empty($name)) {
            return Result::error('请求参数错误', Code::REPOS_NAME_EMPTY);
        }

        $action = $action ?: 'lists';

        $repository = app()->config->get('repos.map.' . $name);

        if (empty($repository)) {
            return Result::error('尚未匹配数据仓库', Code::REPOS_NO_FOUND);
        }
        if (!class_exists($repository)) {
            return Result::error('尚未匹配数据仓库', Code::REPOS_NO_FOUND);
        }

        /**
         * @var Repository $repository
         */
        $repository = new $repository();
        if (!$repository instanceof Repository) {
            return Result::error('数据仓库设置错误', Code::REPOS_NO_FOUND);
        }

        $model = $repository->model();
        if (!$model || !class_exists($model)) {
            return Result::error('数据仓库模型尚未设置', Code::REPOS_NO_FOUND);
        }

        /**
         * @var Model $model
         */
        $model = new $model();
        if (!$model instanceof Model) {
            return Result::error('数据仓库模型设置错误', Code::REPOS_NO_MODEL);
        }

        $output = $repository->output();
        $input = $repository->input();
        if ($action === 'lists') {
            return $this->lists($model, $output);
        }

        if ($action === 'get') {
            return $this->get($model, $output, $params);
        }

        if ($action === 'save') {
            return $this->save($model, $input, $params);
        }

        if ($action === 'create') {
            return $this->create($model, $input, $params);
        }

        if ($action === 'del') {
            $setting = $repository->delete();
            return $this->del($model, $setting, $params);
        }

        return Result::error('请求行为不支持', Code::REPOS_ACTION_ERROR);
    }

    /**
     * @param $model
     * @param OutputSetting $output
     * @param $params
     * @return Result
     */
    protected function get($model, OutputSetting $output, $params): Result
    {
        $primary_key = $output->getPrimaryKey();

        $value = $params[$primary_key] ?? null;
        if ($value === null) {
            return Result::success();
        }

        $fields = $output->getFields();
        if ($fields) {
            $model = $model->field($fields);
        } else {
            $model = $model->withoutField(['updated_at', 'deleted_at']);
        }

        $single = $output->getSingle() ?: $output->getCondition();
        if ($single) {
            $condition = $single;
        }

        $user = $output->getUser();
        if ($user) {
            if (!$this->user_id) {
                return Result::error('用户授权已过期', Code::AUTHORIZATION_ILLEGAL);
            }
            $condition[] = [$user, '=', $this->user_id];
        }

        $condition[] = [$primary_key, '=', $value];

        $data = $model->with($output->getWith())->where($condition)->find();

        $before = $output->getBefore()['get'] ?? null;
        if (is_callable($before)) {
            $data = $before($data);
            if ($data instanceof Result) {
                return $data;
            }
        }

        return Result::success($data);
    }

    /**
     * @param $model
     * @param OutputSetting $output
     * @param array $params
     * @return Result
     */
    protected function lists($model, OutputSetting $output, array $params = []): Result
    {
        try {
            $condition = $output->getCondition();
            $fields = $output->getFields();
            $sort = $output->getSort();
            $with = $output->getWith();
            $paginate = $output->isPaginate();
            $user = $output->getUser();
            if ($user) {
                if (!$this->user_id) {
                    return Result::error('授权已过期', Code::AUTHORIZATION_ILLEGAL);
                }
                $condition[] = [$user, '=', $this->user_id];
            }

            if ($condition) {
                $model = $model->where($condition);
            }

            if ($fields) {
                $model = $model->field($fields);
            } else {
                $model = $model->withoutField(['updated_at', 'deleted_at']);
            }

            if ($sort) {
                $model = $model->order($sort);
            }

            if ($paginate) {
                $page_size = ($params['page_size'] ?? 0) ?: 15;
                if ($page_size > 200) {
                    $page_size = 200;
                }

                $data = $model->with($with)->paginate([
                    'list_rows' => $page_size,
                    'var_page' => 'page_id',
                ]);

                $data = ApiPaginate::api($data);

            } else {
                $data = $model->with($with)->select();
            }

            $before = $output->getBefore()['lists'] ?? null;
            if (is_callable($before)) {
                $data = $before($data);
                if ($data instanceof Result) {
                    return $data;
                }
            }

            return Result::success($data);

        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        return Result::error('获取数据失败', Code::DB_QUERY_ERROR, $message);
    }

    /**
     * @param $model
     * @param InputSetting $input
     * @param array $params
     * @return Result
     */
    protected function insert($model, InputSetting $input, array $params = []): Result
    {

        $primary_key = $input->getPrimaryKey();
        $accept = $input->getAccept();
        if (empty($accept)) {
            return Result::error('非法请求', Code::REPOS_SAVE_PARAMS_EMPTY);
        }

        $data = $input->getData();

        // 这里是提取设置允许接收的字段信息
        $wait_save_data = [];
        foreach ($accept as $item) {
            $item = trim($item);
            if (isset($params[$item])) {
                $wait_save_data[$item] = $params[$item];
            }
        }
        // 跟系统设置的默认字段合并
        $wait_save_data = array_merge($wait_save_data, $data);

        $user = $input->getUser();
        if ($user) {
            $wait_save_data[$user] = $this->user_id;
        }

        // 进行验证
        $validate = $input->getValidate();
        if (empty($validate) || !class_exists($validate)) {
            return Result::error('数据仓库字段验证失败', Code::REPOS_VALIDATE_EMPTY);
        }

        $validate = new $validate();
        if (!$validate instanceof Validate) {
            return Result::error('数据仓库字段验证失败', Code::REPOS_VALIDATE_EXTEND_ERROR);
        }

        if (!$validate->check($wait_save_data)) {
            return Result::error($validate->getError(), Code::VALIDATE_ERROR);
        }

        // 判断是更新还是新增
        $id = $wait_save_data[$primary_key] ?? null;

        // 插入或者更新之前
        $before = $input->getBefore();

        // 更新
        if ($id) {

            $callable = $before['update'] ?? '';
            if (is_callable($callable)) {
                $wait_save_data = $callable($wait_save_data);
                if (!is_array($wait_save_data)) {
                    return $wait_save_data;
                }
            }

            $condition[] = [$primary_key, '=', $id];

            $record = $model->where($condition)->find();
            if (empty($record)) {
                return Result::error('数据记录不存在，更新失败');
            }

            if ($user) {

                if (empty($record[$user] ?? '')) {
                    return Result::error('数据记录不存在，更新失败');
                }

                if ($record[$user] != $this->user_id) {
                    return Result::error('数据记录不存在，更新失败');
                }
                unset($wait_save_data[$user]);
            }

            $record = $model->update($wait_save_data, $condition);

        } else {

            $callable = $before['create'] ?? '';
            if (is_callable($callable)) {
                $wait_save_data = $callable($wait_save_data);
                if (!is_array($wait_save_data)) {
                    return $wait_save_data;
                }
            }

            $record = $model->create($wait_save_data);

            $id = $record->id;
        }

        return $id ? Result::success($record, '保存成功') : Result::error('保存失败');
    }

    /**
     * @param $model
     * @param InputSetting $input
     * @param array $params
     * @return Result|null
     */
    protected function save($model, InputSetting $input, array $params = []): ?Result
    {
        if (!$input->isAllowSave()) {
            return Result::error('非法请求', Code::REPOS_NOT_SUPPORT_SAVE);
        }
        return self::insert($model, $input, $params);
    }

    /**
     * @param $model
     * @param InputSetting $input
     * @param array $params
     * @return Result|null
     */
    protected function create($model, InputSetting $input, array $params = []): ?Result
    {
        if (!$input->isAllowCreate()) {
            return Result::error('非法请求', Code::REPOS_NOT_SUPPORT_SAVE);
        }

        unset($params[$input->getPrimaryKey()]);
        return self::insert($model, $input, $params);
    }

    /**
     * @param $model
     * @param DeleteSetting $setting
     * @param array $params
     * @return Result
     */
    protected function del($model, DeleteSetting $setting, array $params = []): Result
    {
        $allow = $setting->isAllow();
        if (!$allow) {
            return Result::error('非法请求', Code::REPOS_DELETE_REFUSED);
        }

        $primary_key = $setting->getPrimaryKey() ?: 'id';
        $id = $params[$primary_key] ?? '';
        if (empty($id)) {
            return Result::error('非法请求', Code::REPOS_DELETE_RESTRAIN_EMPTY);
        }

        if (empty($this->user_id)) {
            return Result::error('非法请求', Code::REPOS_DELETE_RESTRAIN_EMPTY);
        }

        $user_id = $setting->getUserId();
        $condition = $setting->getCondition();
        $batch = $setting->isBatch();

        $condition[] = [$user_id, '=', $this->user_id];
        if ($batch) {
            $id = explode(',', $id);
            $condition[] = [$primary_key, 'in', $id];
        } else {
            $condition[] = [$primary_key, '=', $id];
        }

        /**
         * @var $records  Collection
         */
        $records = $model->where($condition)->select();
        if ($records->isEmpty()) {
            return Result::error('数据不存在,删除失败', Code::REPOS_DELETE_EMPTY_DATA);
        }

        $before = $setting->getBefore();
        if (is_callable($before)) {
            $records = $before($records);
            if ($records instanceof Result) {
                return $records;
            }
        }

        $soft = $setting->isSoft();
        $success = 0;
        foreach ($records as $item) {
            if ($soft) {
                $res = $item->delete();
            } else {
                $res = $item->force()->delete();
            }
            if ($res) {
                $success++;
            }
        }

        if ($success === 0) {
            return Result::error('删除失败，请重试');
        }

        if ($success === 1) {
            return Result::success($success, '删除完成');
        }

        return Result::success($success, '删除完成，共计删除' . $success . '条');
    }

    /**
     * 记录日志
     * @param $name
     * @param $action
     * @param $user_id
     * @param $params
     * @return false|void
     */
    public function log($name, $action, $user_id, $params)
    {
        $root = app()->config->get('repos.log.root');
        if (!is_dir($root)) {
            return false;
        }

        if (!is_writeable($root)) {
            return false;
        }

        $method = Request::method();

        $str = "\n";
        foreach ($params as $key => $param) {
            $str .= '        ' . $key . ' : ' . $param . "\n";
        }

        $date = date('Y-m-d H:i:s');
        $action = $action ?: 'lists';
        $message = <<<EOF
【{$date}】
    action:$action
    user_id:$user_id
    method:$method
    params:$str
==============end==============

EOF
            . "\n";

        $filepath = $root . $name;
        file_put_contents($filepath, $message, FILE_APPEND);
    }
}