# ThinkPHP6 Development Utils

[gitee](https://gitee.com/magein/think-utils)

[composer](https://packagist.org/packages/magein/think-utils)

[lastest version v2.0.0](https://gitee.com/magein/think-utils/tags)

### 简介

> 使用php框架开发习惯使用到的类以及api接口开发的响应

### 更新日志

#### 2.0.0

1. 新增树结构模型trait
2. 修改目录为thinkphp开发规范
3. 使用extra加载扩展

#### 1.0.2

1. ModelStatusField 名称修改为 StatusTranslate
2. StatusTranslate 新增translateYesOrNo、translateUpOrDown方法
3. StatusConst新增yes、no，up、down常量

#### 1.0.1

1. 修复IsValidate中判断状态时调用常亮的错误

### MainModel说明

#### 快捷查询

Models下面的模型继承MainModel享有额外的查询功能

```php
/**
  * @method static Order|null _orderNo($order_no);
  * @method static \think\paginator\driver\Bootstrap|null __orderNo($order_no); 
  * @method static \think\model\Collection|null ___orderNo($order_no);
  * 
*/

// 查询单个数据 拼接查询条件['order_no'=>$order_no]
$order_no='';
Order::_orderNo($order_no);
// 分页查询 可以在请求中携带page_size参数，也可以在model中重新定义page_size属性值
Order::__orderNo($order_no);
Order::__order_no($order_no);
Order::__order_no__($order_no);

Order::__orderNo($order_no,10);
Order::__orderNo($order_no,['page_size'=>10]);

// 查询全部数据
Order::___orderNo($order_no);
```

#### 快捷方法

检索并且创建

```php
Order::firstOrCreate($where,$params)
```

更新或者创建，会更新满足条件的所有数据

```php
Order::updateOrCreate($where,$params)
```

### SoftModel说明

> 继承MainModel，数据进行软删除

### api数据仓库服务

#### 背景

前后端分离的项目，需要请求api接口，比如请求以下数据

1. 获取首页的banner图（不分页，包含状态、开始时间、结束时间等）
2. 获取商品信息（分页）
3. 用户的订单信息（需要携带当前登录用户的标识进行查询）
4. 用户的收货地址列表（需要携带当前登录用户的标识）
5. 保存用户的收货地址、编辑用户的收货地址

#### 特别说明

> 此功能仅仅是快捷使用，不能包含事务操作，只能替代大多数建议的增删改查功能

#### 请求路由

业务场景：

获取用户的收货地址列表、获取详情、新增、更新、删除（用户需要登录）

    /api/repos/address
    /api/repos/address/get?id=xxx
    /api/repos/address/save
    /api/repos/address/create
    /api/repos/address/del?id=xxx

获取商品列表、详情、以及其他相关的属性(用户不需要登录)

    /api/repos/goods
    /api/repos/goods/get?id=xxx

#### 模型文件

用户收货地址表名称为 user_address

```php
php think model:new user_address
```

收货地址的模型文件

    app/model/UserAddress.php

#### ~~注册路由~~

v2~版本中已经支持composer extra加载不需要手动注册

> append中必须包含user_id且值不能为空，用于标记当前登录用户

```php
Route::any('repos/:name/[:action]', '\magein\think\utils\ApiRepos@entry')
    ->append(['user_id' => '获取登录用户的id']);
```

#### 生成数据仓库

```php
php think model:repos user_address
```

在app/api/Repository目录下生成 UserAddressRepos.php

```php

namespace app\api\repository;

use magein\think\utils\repository\InputSetting;
use magein\think\utils\repository\OutputSetting;
use magein\think\utils\repository\DeleteSetting;
use magein\think\utils\repository\Repository;

use app\model\UserAddress;

class UserAddressRepos extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return UserAddress::class;
    }

    /**
     * @return OutputSetting
     */
    public function output(): OutputSetting
    {
        $setting = new OutputSetting();
        $setting->setPaginate();
        $setting->setSort('id desc');

        return $setting;
    }
    
    /**
     * @return InputSetting
     */
    public function input(): InputSetting
    {
        $accept = [
            'id',
            'user_id',
            'province',
            'city',
            'area',
            'phone',
            'name'
        ];
        $setting = new InputSetting();
        $setting->setAccept($accept);
        
        return $setting;
    }

    /**
     * @return DeleteSetting
     */
    public function delete(): DeleteSetting
    {
        $setting = new DeleteSetting();
        $setting->allow();
        return $setting;
    }
}
```

model()方法指定使用的模型

#### 添加匹配

在app/api/config文件中添加repos.php

```php
return [
    'map' => [
        'address'=> \app\api\repository\UserAddressRepos::class,
    ],
];
```

#### 输出

包含输出单个（详情）、列表、分页三种情况

通过output() 方法返回的： OutputSetting 控制 请求列表或详情

```php
public function output(): OutputSetting
{
    $setting = new OutputSetting();
    // 请求是否分页
    $setting->setPaginate();
    // 设置排序
    $setting->setSort('id desc');
    // 设置用户且关联的表字段为 user_id 值为路由表中添加的参数
    $setting->setUser();
    // 设置用户且关联的表字段为 member_id值为路由表中添加的参数
    $setting->setUser('member_id');
    // 设置用户且关联的表字段为 null 表示不需要关联用户的数据，比如首页的banner图、活动图、商品、等公开的信息，不需要登录即可获取
    $setting->setUser(null);
    // 设置关联模型 需要再模型中设定，跟thinkphp的关联模型一致，这里只是取值
    $setting->setWith(['user']);
    // 设置返回给前端的字段信息，这里不能控制关联表中的数据,需要再模型中设置关联模型显示那些数据
    $setting->setFields([]);
    // 设置查询条件 比如状态 上下架 是否审核等等
    $setting->setCondition('status',1);
    $setting->setCondition('status','>'1);
    $setting->setCondition('start_time','<=',time())
    $setting->setConditions(['status'=>1,'type'=>1])
    $setting->setConditions([
        ['status','=',1],
        ['type','in',[1,2]]
    ]);
    // 获取单条数据使用的条件 用法同setCondition
    $setting->setSingle();
    // 获取单条数据使用的条件 用法同setConditions
    $setting->setSingles();
    // 设置主键 默认是id，用于获取单条数据的标识，如根据订单编号order_no获取详情,根据商品编号获取详情good_no
    $setting->setPrimaryKey();
    // 获取单个数据的回调
    $setting->simpleBefore(function($data){});
    // 获取列表数据的回调
    $setting->listsBefore(function($data){});
    // 返回数据前的回调，这里需要返回的将作为接口数据返回，设置后获取列表或者单条信息，都将执行
    $setting->before(function($data){
    
    // 添加自己的业务逻辑，然后直接返回
    if (!request()->get('flag')){
        return magein\utils\Result::error('参数错误');
    }
    
    // 判断是否是get请求（详情数据）
    if (request()->route('action')=='get'){
       return magein\utils\Result::error('参数错误');
    }
    
    // 直接输出给前端
    return magein\utils\Result::success($data);
    
     // $data是查询出来的数据,添加气的业务逻辑后再返回
     $data['other']=[1];
     
     return $data;
    });

    return $setting;
}
```

#### 输入

包含新增、更新

通过 input() 方法返回的： InputSetting 控制 请求列表或详情

```php
public function input(): InputSetting
{
    $setting = new InputSetting();
    // 设置主键 默认是id，用于判断是更新还是新增的标识字段。默认id 此键携带了值表示更新，同时会验证user
    $setting->setPrimaryKey();
     // 设置用户且关联的表字段为 user_id，插入或者更新的时候要判断数据是否输出此键对应的值，即判断是否属于当前登录用户的，且必须要设置
    $setting->setUser();
    // 设置接收参数，即提交的表单有那些参数是合法的
    $setting->setAccept();
    // 设置验证表单数据的验证类
    $setting->setValidate();
    // 设置额外的字段信息，将于接收到的参数合并，且作为插入或者更新数据
    $setting->setData();
    // 允许执行插入或者更新操作
    $setting->allowSave();
    // 允许更新，当只能插入且不能更新的时候需要设置
    // $setting->allowSave(false)
    $setting->allowCreate();
    // 插入之前的回调，这里如果返回的数组，则执行插入，非数组类则将返回值直接返回给前端
    $setting->createBefore();
    // 执行更新前的回调，返回值同上
    $setting->updateBefore();
    
    return $setting;
}
```

#### 删除

delete方法控制删除，默认是不允许执行删除操作

删除只针对用户操作自己的，所以需要设置用户的关联字段

```php
public function delete(): DeleteSetting
{
    $setting = new DeleteSetting();
    // 允许删除
    $setting->allow();
    // 设置用户的关联字段，默认是user_id
    $setting->setUserId('member_id');
    // ?oid=1 将会在参数中取出id的值且拼接到删除的条件中 默认是id
    $setting->setPrimaryKey('oid');
    return $setting;
}
```

上诉声明得到的删除条件是

```php
$condition=[
    [
        'oid'=>request()->get('oid'),
        'member_id'=>'登录用户的id'
    ]
];
```

```php
public function delete(): DeleteSetting
{
    $setting = new DeleteSetting();
    // 允许删除
    $setting->allow();
    // 设置用户的关联字段，默认是user_id
    $setting->setUserId('member_id');
    // ?oid=1 将会在参数中取出id的值且拼接到删除的条件中 默认是id
    $setting->setPrimaryKey('oid');
    // 设置是否允许批量删除  批量删除参数是以逗号隔开的字符串 如：?oid=1,2,3
    $setting->setBatch();
    // 默认是软删除 关闭软删除则进行物理删除
    $setting->setSoft(false);
    // 通输入、输出的条件设置
    $setting->setCondition();
    $setting->setConditions();
    // 删除之前的回调 $data 是根据条件查询出来的集合
    $setting->before(function($data)){
       
       // 将不进行删除操作
       return magein\utils\Result::error('删除错误');
    
       // 继续往下执行删除操作
       return $data;
    }
    
    return $setting;
}
```









